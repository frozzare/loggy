#[desc = "A simple logger written in Rust"];
#[license = "MIT"];

pub struct Entry {
	env: ~str,
	message: ~str,
	level: ~str,
	color: int
}

pub struct loggy {
	env: ~str
}

fn format (entry: Entry) {
	println!(
		"\x1b[{}m{}\x1b[0m {}",
		entry.color,
		entry.level,
		entry.message
	);
}

impl loggy {
	pub fn new (env: ~str) -> loggy {
		loggy { env: env }
	}
	pub fn info (&self, message: ~str) {
		format(Entry{
			env: ~"",
			message: message,
			level: ~"INFO",
			color: 34
		});
	}
	pub fn warn (&self, message: ~str) {
		format(Entry{
			env: ~"",
			message: message,
			level: ~"WARN",
			color: 33
		});
	}
	pub fn error (&self, message: ~str) {
		format(Entry{
			env: ~"",
			message: message,
			level: ~"ERR!",
			color: 31
		});
	}
}