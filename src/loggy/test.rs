#[feature(globs)];
#[license = "MIT"];
extern mod loggy;
use loggy::*;

#[test]
fn test_loggy () {
	let log = loggy::new(~"development");
	log.info(~"hello, world");
	log.warn(~"hello, world");
	log.error(~"hello, world");
}